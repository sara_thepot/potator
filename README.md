<h1>Potator</h1>

Work in progress ;)

<h2>Getting started</h2>

The optimal HTML structure is:
	
	<div id="yourRotatorID" class="potator">
		<img src="#" class="your1stImage">
		<img src="#" class="your2ndImage">
		<img src="#" class="your3rdImage">
		[...]
	</div>


Initialize it with:

    <script type="text/javascript">
		var myRotator = new potator(document.getElementById("yourRotatorID"));
    </script>


<h2 id="license">License (MIT)</h2>

Copyright (c) 2016 Sara Potyscki, [thepot.site](http://www.thepot.site/portfolio/potator)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
