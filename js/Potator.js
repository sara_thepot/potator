/* 
 * Potator
 *
 * Version: 1.1
 *  Author: Sara Potyscki (ThePot)
 * Website: http://www.thepot.site
 * License: Released under the MIT license [http://www.thepot.site]
 *
 */


(function (d,w) {
    'use strict';
		
	w.potator = function(divContainer, options = null){
		if (options == null) {
			options = {"nothing": true};
		}
		return this.init(divContainer, options);
	};
	
	potator.prototype = {
		_divContainer : null,
		_images : null,
		_initialMouseX: null,
		_currentMouseX: null,
		_spinWidth: 0,
		_frameWidth: 0,
		_currentIndex : 0,
		_previousIndex : null,
		_initialized: false,
		_dragging: false,
		_callback : function(){},
		
		options : {
			spins : 5, 
			callbackFunction : function(){} 
		},
		init : function(divContainer, options){
			
			for(var i in options){
				this.options[i] = options[i];
			}
			this._divContainer = divContainer;
			this._images = this._divContainer.querySelectorAll("img");
			
			this._divContainer.setAttribute("class",this._divContainer.getAttribute("class")+" loading");

			this._checkLoading();
			
			var _this = this;
			window.onresize = function(){
				_this._onWindowResize();
			};
			
			
		},
		_checkLoading : function(){
			var imageCount = this._images.length;
			var imagesLoaded = 0;
			
			var _this = this;
			
			for(var i=0; i<imageCount; i++){
				if(this._images[i].complete){
					imagesLoaded++;
				}else{
				
					this._images[i].onload = function(){
						imagesLoaded++;
						if(imagesLoaded == imageCount){
							_this._initialized = true;
							_this._initialize();	
						}
					};
				}
			}
			
			if(imagesLoaded == imageCount){
				_this._initialized = true;
				_this._initialize();	
			}
					
		},
		_initialize : function(){
			
			var _this = this;
			
			//setto l'eventuale _currentIndex personalizzato
			
			for(var i = 0; i < this._images.length; i++){
				if (i == this._currentIndex) {
					this._images[i].style.display = "initial";
				}else{
					this._images[i].style.display = "none";
				}
			}
			
			this._spinWidth = this._divContainer.offsetWidth / this.options.spins;
			this._frameWidth = this._spinWidth / this._images.length;
			
			this._divContainer.setAttribute("class", this._divContainer.getAttribute("class").replace("loading", ""));
			
	
			var fnStart = function(e){
				e.stopPropagation(); 
				_this._dragging = true;
				if(e.offsetX){
					_this._initialMouseX = e.offsetX;
					_this._currentMouseX = e.offsetX;
				}else{
					var rect = e.target.getBoundingClientRect();
					_this._initialMouseX = e.targetTouches[0].pageX - rect.left;
					_this._currentMouseX = e.targetTouches[0].pageX - rect.left;
				}
			};
			this._divContainer.addEventListener('mousedown', fnStart);
			this._divContainer.addEventListener('touchstart', fnStart);
			
			
			var fnMove = function(e){
				e.stopPropagation(); 
				if (_this._dragging) {
					if(e.offsetX){
						_this._currentMouseX = e.offsetX;
					}else{
						var rect = e.target.getBoundingClientRect();
						_this._currentMouseX = e.targetTouches[0].pageX - rect.left;
					}
					_this._previousIndex = _this._currentIndex;
					
					var range = _this._currentMouseX - _this._initialMouseX;
					
					_this._currentIndex = parseInt((range % _this._spinWidth) / _this._frameWidth);
					
					if (_this._currentIndex < 0) {
						_this._currentIndex = _this._currentIndex + _this._images.length;
					}
					_this._render();
					
				}
			}
			this._divContainer.addEventListener('mousemove', fnMove);
			this._divContainer.addEventListener('touchmove', fnMove);
			
			var fnEnd = function(e){
				e.stopPropagation(); 
				_this._dragging = false;
			};
			this._divContainer.addEventListener('mouseup', fnEnd);
			this._divContainer.addEventListener('touchend', fnEnd);			
			
			
			this.options.callbackFunction();
		},
		_render : function(){
			this._images[this._currentIndex].style.display = "initial";
			if (this._previousIndex != this._currentIndex) {
				this._images[this._previousIndex].style.display = "none";
			}

			
			
		},
		_onWindowResize : function(){
			
			if (this._initialized) {
				this._divContainer.setAttribute("class",this._divContainer.getAttribute("class")+" loading");
				this._initialize();			
			}
			
		}
	};
	
	
})(document,window);

